#!/bin/sh

getent group dhcpcd || /usr/sbin/groupadd -r dhcpcd
getent passwd dhcpcd || /usr/sbin/useradd -d /var/lib/dhcpcd -g dhcpcd -s /bin/false dhcpcd
/usr/bin/passwd -q -l dhcpcd
